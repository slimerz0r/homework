var counter = 0;
var timer;
var reversetimer;
var count = function () {
  counter+=6;
  if (counter <= 180){
    document.getElementById("myDiv").style.transform = "rotate(" + counter + "deg)"
    document.body.style.background = "#"+((1<<24)*Math.random()|0).toString(16);
  } else {
    clearInterval(timer);
    startb.removeAttribute('disabled','');
  }
};

var countReverse = function() {
  counter-=6;
  if (counter >= -180){
    document.getElementById("myDiv").style.transform = "rotate(" + counter + "deg)"
    document.body.style.background = "#"+((1<<24)*Math.random()|0).toString(16);
  } else {
    clearInterval(reversetimer);
    revb.removeAttribute('disabled','');
    };
};

var start = function() {
  if(reversetimer){
    clearInterval(reversetimer);
    revb.removeAttribute('disabled','');
  }
  timer = setInterval(count, 1000);
  startb.setAttribute('disabled','');
};

var reverse = function() {
  reversetimer = setInterval(countReverse, 1000);
  if(timer){
    clearInterval(timer);
    startb.removeAttribute('disabled','')
  }
  revb.setAttribute('disabled','');
}

function reset() {
  counter = 0;
  document.body.style.background = "white";
  document.getElementById("myDiv").style.transform = "rotate(" + counter + "deg)"
};

var stop = function () {
  clearInterval(timer);  
  clearInterval(reversetimer);
  revb.removeAttribute('disabled','');
  startb.removeAttribute('disabled','');
};