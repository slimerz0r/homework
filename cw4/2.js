    window.addEventListener("load", function () {
      var myWidth = document.getElementById('myWidth');
      var myHeight = document.getElementById('myHeight');
      var myBorderRadius = document.getElementById('myBorderRadius');
      var myMargin = document.getElementById('myMargin');
      var myColor = document.getElementById('myColor');
      // var randomColor = function() {
      // 	var letters = '0123456789ABCDEF'.split('');
      // 	var color = '#';
      // 	for (var i = 0; i < 6; i++ ) {
      // 	  color += letters[Math.round(Math.random() * 15)];
      // 	}
      // 	return color;
      // } ...может потом

      myWidth.addEventListener('input', function(event){
        result.style.width = event.target.value + 'px';
      });
      myColor.addEventListener('input', function(event){
      	result.style.backgroundColor = event.target.value;
      })
      myHeight.addEventListener('input', function(event){
        result.style.height = event.target.value + 'px';
      });
      myBorderRadius.addEventListener('input', function(event){
        result.style.borderRadius = event.target.value + 'px';
      });
      myMargin.addEventListener('input', function(event){
        result.style.margin = event.target.value + 'px';
      });
    });
