document.addEventListener('DOMContentLoaded', function () {
	var formElements = Array.from( MyValidateForm.elements );

	formElements.forEach( function(item){
		if( item.type !== 'checkbox' ){
			item.addEventListener('input', function(e) {
				if(item.value){
					item.classList.add('OK');
				} else {
					item.classList.remove('OK');
					item.classList.add('error');
				}
			});
		};
	});

	agree.addEventListener('click', function(e){
		if(e.target.checked) {
			MyValidateForm.submit.removeAttribute('disabled');
		} else {
			MyValidateForm.submit.setAttribute('disabled','');
		};
	})
});

MyValidateForm.addEventListener('submit', function(e){
  e.preventDefault();
  if (MyValidateForm.pas1.value === MyValidateForm.pas2.value) {
  	passCondition = 1;
  } else {
  	passCondition = 0;
  };
  if (passCondition) {
  	MyValidateForm.pas1.classList.remove('error');
  	MyValidateForm.pas2.classList.remove('error');
  	MyValidateForm.pas1.classList.add('OK');
  	MyValidateForm.pas2.classList.add('OK');
  } else {
    MyValidateForm.pas1.classList.add('error');
    MyValidateForm.pas2.classList.add('error');
  	MyValidateForm.pas1.classList.remove('OK');
  	MyValidateForm.pas2.classList.remove('OK');
    alert('Пароли не совпадают');
  }
});
