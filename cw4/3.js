window.addEventListener("load", function () {
	var toDoList = document.getElementById('toDoList');
	var newToDo = document.getElementById('newToDo');
	var AddToDo = document.getElementById('AddToDo');
	var listCount = 0;
	var checkList = function() {
		if (toDoList.childElementCount > 4) {
			AddToDo.setAttribute('disabled', '');
		} else {
			AddToDo.removeAttribute('disabled', '');
		};
	};
	AddToDo.addEventListener('click', function(e){
		var li = document.createElement('li');
		if (newToDo.value) {
			li.className = 'listItem';
			var liInput = document.createElement('input');
				liInput.type = 'checkbox';
				liInput.className = 'DoneCheckBox';
			li.appendChild(liInput);
			var liSpan = document.createElement('span');
				liSpan.className = 'TodoText';
				liSpan.innerText = newToDo.value;
			li.appendChild(liSpan);
			var liButton = document.createElement('button');
				liButton.innerText = 'remove';
				liButton.onclick = function(e){
					e.path[1].remove();
					checkList();
				}
			li.appendChild(liButton);
			toDoList.appendChild(li);
			newToDo.value = '';
			checkList();
		};
	});
});