
  /*

    Задание 1.

    Написать скрипт который будет будет переключать вкладки по нажатию
    на кнопки в хедере.

    Главное условие - изменять файл HTML нельзя.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
         + бонус выбрать одним селектором

      2. Повесить кнопку онклик
          button1.onclick = function(event) {

          }
          + бонус: один обработчик на все три кнопки

      3. Написать функцию которая выбирает соответствующую вкладку
         и добавляет к ней класс active

      4. Написать функцию hideAllTabs которая прячет все вкладки.
         Удаляя класс active со всех вкладок

    Методы для работы:

      getElementById
      querySelector
      classList
      classList.add
      forEach
      onclick

      element.onclick = function(event) {
        // do stuff ...
      }

  */

var btn = document.getElementsByTagName('button');
var tb = document.querySelectorAll('.tab');

for (i = 0; i < btn.length; i++) {
  btn[i].setAttribute('onclick', 'showTab(' + i + ')');
};

function hideAllTabs () {
  for (i = 0; i < tb.length; i++) {
  tb[i].classList.remove('active');
  };
};

function showTab (n) {
  if (tb[n].classList.contains('active')) {
    hideAllTabs();
  } else {
    hideAllTabs();
    tb[n].classList.add('active');
  }
};
