/*

  Задание:

    Написать при помощи //async-await 
    ----->>> через fetch <<<-----
    скрипт, который:

    Получает список компаний:  http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2
    Перебирает, выводит табличку:

    Company | Balance | Показать дату регистрации | Показать адресс |
    1. CompName 2000$ button button

    Данные о дате регистрации и адресс скрывать при выводе и показывать при клике.

*/
let counter = 0;
let counter2 = 0;
let o = {};
let url = "http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2";
// let test = new Promise (function())
fetch (url).then (res => {
	return res.json();
}).then (res => {
	res.forEach(function(item){
		counter++;
	    let tr = document.createElement('tr');
	    	tr.id = counter-1;
	   	let td = document.createElement('td');
	   		td.innerHTML = counter;
	   	tr.appendChild(td);
	   		td = document.createElement('td');
	   		td.innerHTML = item.company;
	   	tr.appendChild(td);
	   		td = document.createElement('td');
	   		td.innerHTML = item.balance;
	   	tr.appendChild(td);
	   		td = document.createElement('td');
	   		td.id = ("registered" + counter);
	   		td.addEventListener('click', function() {
	   			if ( document.getElementById("registered"+(+(this.parentElement.id) + 1)).innerHTML == ('<button>show date</button>') ) {
	   				document.getElementById("registered"+(+(this.parentElement.id) + 1)).innerHTML = "<button>hide date</button>";
	   				document.getElementById("registered"+(+(this.parentElement.id) + 1)).innerHTML += res[this.parentElement.id].registered;
	   			} else {
	   				document.getElementById("registered"+(+(this.parentElement.id) + 1)).innerHTML = "<button>show date</button>";
	   				console.log(document.getElementById("registered"+(+(this.parentElement.id) + 1)).innerHTML)
	   			}
	   		});
	   		td.innerHTML = ("<button>show date</button)");
	   	tr.appendChild(td);
	   		td = document.createElement('td');
	   		td.id = ("address" + counter);
	   		td.addEventListener('click', function() {
	   			if ( document.getElementById("address"+(+(this.parentElement.id) + 1)).innerHTML == ('<button>show address</button>') ){
	   				document.getElementById("address"+(+(this.parentElement.id) + 1)).innerHTML = "<button>hide address</button>"
	   				for (prop in res[this.parentElement.id].address) {
						document.getElementById("address"+(+(this.parentElement.id) + 1)).innerHTML += (prop + ": " + res[this.parentElement.id].address[prop]);
	   				}
	   			} else {
	   				document.getElementById("address"+(+(this.parentElement.id) + 1)).innerHTML = "<button>show address</button>";
	   			}
	   		});
	   		td.innerHTML = "<button>show address</button)";
	   	tr.appendChild(td);
	document.getElementById('table').appendChild(tr);
})});
