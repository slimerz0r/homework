    let id_counter = 0;
    let global_messages = [];

    class Message {
      constructor(author, text){
        //id, author, text, date, answers
        this.id = id_counter++;
        this.author = author;
        this.text = text;
        this.date = new Date();
        this.answers = [];
        global_messages.push( this );
        this.skipMessage = this.skipMessage.bind(this);
        this.answerMessage = this.answerMessage.bind(this);

      }
      skipMessage(){
        event.preventDefault();
              var skip = Array.from(document.querySelectorAll('._skipMessage'));
              skip.forEach(function(item){
                item.addEventListener('click', function(e){
                  e.path[2].remove();
                })
              })

      }
      answerMessage(){
        let x = new Answer('Author', 'Text', this.id);
      }
      sendAnswer(){
        this.andwers.push()
      }

      render(){
        let renderTarget = document.getElementById("message_list");

        let li = document.createElement('li');
            li.innerHTML = `
              <div class="message__date">
                ${this.date}
              </div>
              <div class="message__author">
                <b>${this.author}</b>
              </div>
              <div class="message__text">
                ${this.text}
              </div>
              <div class="message__controls">
                <button class="_skipMessage" onclick="skipMessage()">Skip</button>
                <button class="_answerMessage">Answer</button>
              </div>
              <ul>
                ${
                  this.answers.map( item => item.render() )
                }
              </ul>
              `
              let skipBtn = li.querySelector('._skipMessage');
              skipBtn.addEventListener('click', this.skipMessage )

        renderTarget.appendChild(li);
      }
    }

    class Answer extends Message {
      constructor(author, text, parentId){
        super( author, text);
        this.parentId = parentId
      }
      render(){
        return (`<div>
          ${this.parentId}. ${this.author}
        </div>`)

      }
    }

    new Message ('Author', 'text');

    global_messages.map( item => item.render() )